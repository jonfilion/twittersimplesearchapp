var express = require('express');
var app = express();
var bodyparser = require('body-parser');
var tweetList = require('./tweets');

app.set('view engine', 'ejs');
app.use(bodyparser.urlencoded({ extended: false }));
app.use(express.static(__dirname + "/css"));

app.get('/', function(req, res) {
    res.render('index');
});

app.post('/getResults', function(req, res) {
    var keyword = req.body.searching;
    var language = req.body.language;
    var rType = req.body.rType;

    var tweets = tweetList(res, keyword, language, rType);
})

var server = app.listen(process.env.PORT || 3003, function() {
    console.log("Our app running on 3003");
});