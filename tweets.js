var Twit = require('twit');
var config = require('./config')

module.exports = function(res, query, language, rType) {

    var T = new Twit(config);

    var usersArray = new Array();

    T.get('search/tweets', {
            q: query,
            count: 15,
            result_type: rType,
            lang: language
        },
        function(err, data, response) {
            if (err)
                throw err;

            var statuses = data.statuses;
            for (var i = 0; i < statuses.length; i++) {

                var screenName = statuses[i].user.screen_name;
                var tweetText = statuses[i].text;
                var tweetRetweetCount = statuses[i].retweet_count;
                var tweetFavoriteCount = statuses[i].favorite_count;
                var profileImage = statuses[i].user.profile_image_url;
                var individual = {
                    screen_name: screenName,
                    text: tweetText,
                    retweet_count: tweetRetweetCount,
                    favorite_count: tweetFavoriteCount,
                    profile_image: profileImage
                };
                console.log(individual.profile_image);
                usersArray.push(individual);

            }

            res.render('tweetList', { users: usersArray });
        })
}